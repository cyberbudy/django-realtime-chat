from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import BasePermission

from .models import Conversation


class UserInConversation(BasePermission):

    message = _("There is no chat with such id or you don't have access to it")

    def has_permission(self, request, view):
        if hasattr(view, "conversation_kwarg"):
            conversation_id = request.get(view.conversation_kwarg)
        elif hasattr(view, "conversation_arg"):
            conversation_id = request.query_params.get(view.conversation_arg)
        else:
            conversation_id = request.data.get("conversation_id")

        conversation = Conversation.objects.filter(
            id=conversation_id, interlocutors__user=request.user,
        )

        return conversation.exists()

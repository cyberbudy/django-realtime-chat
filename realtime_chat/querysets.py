from typing import List, Union
from uuid import UUID

from django.db import models

from .const import MESSAGE_STATUSES


class ConversationQuerySet(models.QuerySet):
    def with_only_interlocutors(self, interlocutors: List[int]):
        """
        Find conversation with given interlocutors.

        :param interlocutors: List of user ids to find their conversation
        """
        from .models import Interlocutor

        not_interlocutors = (
            Interlocutor.objects
            .filter(conversation_id=models.OuterRef('pk'))
            .exclude(user_id__in=interlocutors)
            .only("id") # Limit exists query to one column
        )
        return (
            self.filter(interlocutors__user_id__in=interlocutors)
            .annotate(
                interlocutors_count=models.Count("interlocutors__user_id"),
            )
            .filter(~models.Exists(not_interlocutors))
            .filter(interlocutors_count=len(interlocutors))
        )


class InterlocutorQuerySet(models.QuerySet):
    def not_read_message(self, message_id: Union[str, UUID], message_author: int):
        """
        Filters interlocutors that has not read message.

        :param message_id: Message id.
        :param message_author: Message author.
        """
        from .models import MessageStatus

        message_status_not_read = MessageStatus.objects.filter(
            message_id=message_id,
            status__lt=MESSAGE_STATUSES._identifier_map["read"],
            interlocutor=models.OuterRef("pk"),
        ).values("id")

        return self.exclude(user_id=message_author).filter(
            models.Exists(message_status_not_read)
        )


from django.contrib.auth import get_user_model
from rest_framework import filters
from rest_framework.pagination import CursorPagination
from rest_framework.permissions import IsAuthenticated

from standards.drf.pagination import (
    StandardPaginationMixin,
    pagenumber_pagination,
)
from standards.drf.views import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    UpdateAPIView,
)

from .models import Conversation, Message, Interlocutor
from .permissions import UserInConversation
from .serializers import (
    ConversationSerializer,
    MessageSerializer,
    MessageUpdateSerializer,
    NewFileMessageSerializer,
    UserSearchSerializer,
)
from .services import delete_message, update_message, send_new_message

__all__ = (
    "UserConversationList",
    "ConversationMessageListAPIView",
    "UserSearchAPIView",
    "FileMessageCreateAPIView",
)


class ConversationCursorPagination(StandardPaginationMixin, CursorPagination):
    page_size = 5
    page_size_query_param = "page_size"
    ordering = "-last_message_at"

    def get_pagination_info(self, data):
        return {
            "next": self.get_next_link(),
            "previous": self.get_previous_link(),
            "page_size": self.page_size,
        }


class MessageCursorPagination(StandardPaginationMixin, CursorPagination):
    page_size = 20
    page_size_query_param = "page_size"
    ordering = "-created_at"

    def get_pagination_info(self, data):
        return {
            "next": self.get_next_link(),
            "previous": self.get_previous_link(),
            "page_size": self.page_size,
        }


class UserConversationSearchFilter(filters.SearchFilter):
    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)

        q = super().filter_queryset(request, queryset, view)

        if search_fields and search_terms:
            q |= queryset.filter(
                interlocutors__in=Interlocutor.objects.filter(
                    user__username__icontains=search_terms[0]
                ).exclude(user_id=request.user.id)
            )

        return q


class UserConversationList(ListAPIView):
    queryset = Conversation.objects.prefetch_related("interlocutors")
    permission_classes = (IsAuthenticated,)
    serializer_class = ConversationSerializer
    pagination_class = ConversationCursorPagination
    filter_backends = [UserConversationSearchFilter]
    search_fields = ["title"]

    def get_queryset(self):
        queryset = super().get_queryset()

        return (
            queryset.select_related("last_message")
            .prefetch_related(
                "interlocutors",
                "interlocutors__user",
                "interlocutors__user__status",
                "last_message__statuses",
            )
            .filter(interlocutors__user=self.request.user)
        )


class UserSearchAPIView(ListAPIView):
    queryset = (
        get_user_model().objects.all().order_by(get_user_model().USERNAME_FIELD)
    )
    permission_classes = (IsAuthenticated,)
    filter_backends = [filters.SearchFilter]
    search_fields = ["email", "username"]
    serializer_class = UserSearchSerializer
    pagination_class = pagenumber_pagination(
        page_size=10, page_query_param="page"
    )

    def get_queryset(self):
        return super().get_queryset().exclude(id=self.request.user.id)


class FileMessageCreateAPIView(CreateAPIView):
    # NOTE: This view works only if multipart or fileupload parser's
    # included in the settings
    # https://www.django-rest-framework.org/api-guide/parsers/#fileuploadparser
    queryset = Message.objects.all()
    permission_classes = (IsAuthenticated, UserInConversation)
    serializer_class = NewFileMessageSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["user"] = self.request.user

        return context

    def perform_create(self, serializer):
        instance = serializer.get_message_instance(serializer.validated_data)
        send_new_message(instance)


class ConversationMessageListAPIView(ListAPIView):
    queryset = Message.objects
    conversation_arg = "conversation_id"
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated, UserInConversation]
    pagination_class = MessageCursorPagination

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                conversation_id=self.request.query_params.get(
                    self.conversation_arg
                )
            )
            .prefetch_related("statuses")
            .order_by("-created_at")
        )


class MessageUpdateAPIView(UpdateAPIView):
    queryset = Message.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = MessageUpdateSerializer

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(author_id=self.request.user.id, text__isnull=False)
        )

    def perform_update(self, serializer):
        serializer.instance.text = serializer.validated_data.get("text")
        update_message(serializer.instance)


class MessageDestroyAPIView(DestroyAPIView):
    queryset = Message.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = MessageSerializer

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(author=self.request.user.id, deleted_at__isnull=True)
        )

    def perform_destroy(self, instance: Message):
        delete_message(instance.id, instance.conversation_id)

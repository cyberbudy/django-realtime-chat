from django.urls import include, path

from . import views

app_name = "chat"

urlpatterns = [
    path(
        "api/v1/",
        include(
            [
                path(
                    "chat/conversations/list",
                    views.UserConversationList.as_view(),
                    name="conversation_list"
                ),
                path(
                    "chat/search/user",
                    views.UserSearchAPIView.as_view(),
                    name="search_user"
                ),
                path(
                    "chat/conversations/send-file",
                    views.FileMessageCreateAPIView.as_view(),
                    name="send_file"
                ),
                path(
                    "chat/messages/list",
                    views.ConversationMessageListAPIView.as_view(),
                    name="message_list"
                ),
                path(
                    "chat/messages/<uuid:pk>/delete",
                    views.MessageUpdateAPIView.as_view(),
                    name="update_message"
                ),
                path(
                    "chat/messages/<uuid:pk>/update",
                    views.MessageDestroyAPIView.as_view(),
                    name="destroy_message"
                ),
            ],
        ),
    )
]

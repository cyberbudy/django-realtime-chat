from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.module_loading import import_string

from .models import UserStatus
from .conf import EVENT_HANDLERS, MESSAGE_HANDLERS


@receiver(post_save, sender=get_user_model())
def create_user_status(sender, instance, created, **kwargs):
    """
    Creates user status for a new user.
    """

    if created:
        UserStatus.objects.create(user=instance)


def load_settings():
    """
    Load handlers on application initialization.
    """

    for key, value in EVENT_HANDLERS.items():
        EVENT_HANDLERS[key] = import_string(value)()

    for key, value in MESSAGE_HANDLERS.items():
        MESSAGE_HANDLERS[key] = import_string(value)()

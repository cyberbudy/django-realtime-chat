from typing import List, Tuple, Union
from uuid import UUID

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db import models, transaction
from django.utils import timezone

from .const import MESSAGE_STATUSES
from .conf import NOTIFY_ALL_USER_STATUS_CHANGE
from .models import (
    Conversation,
    Interlocutor,
    Message,
    MessageStatus,
    UserStatus,
)
from .serializers import (
    ConversationSerializer,
    InterlocutorUnreadSerializer,
    MessageSerializer,
    MessageStatusUpdateSerializer,
    UserStatusChangedSerializer)
from .utils import format_websocket_message, get_user_group_channel_name

USER_CONNECTIONS_STRING = "user_connections_{}"

UserModel = get_user_model()


def user_connected(user: "UserModel") -> bool:
    """
    Update user's connections amount and returns whether the user has active
     websocket connections.

    :param user: User object
    """

    if user.is_anonymous:
        return False

    try:
        cache.incr(USER_CONNECTIONS_STRING.format(user.id))
    except ValueError:
        cache.set(USER_CONNECTIONS_STRING.format(user.id), 1)

    update_user_status(user, True)

    return True


def user_disconnected(user: "UserModel") -> bool:
    """
    Update user's connections amount and returns whether the user has no active
     websocket connections.

    :param user: User object
    """

    if user.is_anonymous:
        return False

    try:
        user_connections = cache.decr(USER_CONNECTIONS_STRING.format(user.id))
    except ValueError:
        # It's weird no such key in cache
        cache.set(USER_CONNECTIONS_STRING.format(user.id), 0)
        user_connections = 0

    # If user closed all his connections - set his status as offline
    if not user_connections:
        update_user_status(user, False)
        return True

    return False


def update_user_status(user: "UserModel", is_online: bool):
    # No user status exists -> create it. Not so good in case of handling
    # tons of requests but it's supposed that user status is created on
    # user creation
    UserStatus.objects.update_or_create(
        user_id=user.id, defaults={'is_online': is_online}
    )


def send_new_message(message: Message):
    """
    Operates on new message receiving.

    :param message: New message instance.
    """

    # Don't wait in transaction for message sending
    with transaction.atomic():
        message.save()

        interlocutors = Interlocutor.objects.filter(
            conversation_id=message.conversation_id
        )

        # Increment unread counter
        interlocutors.exclude(user_id=message.author_id).update(
            unread=models.F("unread") + 1
        )

        interlocutors = list(interlocutors)

        # Update conversation info
        Conversation.objects.filter(id=message.conversation_id).update(
            last_message=message.id, last_message_at=message.created_at
        )

        # Add corresponding message statuses for each interlocutor
        MessageStatus.objects.bulk_create(
            [
                MessageStatus(
                    message=message,
                    interlocutor=interlocutor,
                    user_id=interlocutor.user_id,
                    status=(
                        MESSAGE_STATUSES.read
                        if interlocutor.user_id == message.author_id
                        else MESSAGE_STATUSES.created
                    ),
                )
                for interlocutor in interlocutors
            ]
        )

    channel_layer = get_channel_layer()
    message_info = MessageSerializer(instance=message).data

    for interlocutor in interlocutors:
        if interlocutor.user_id != message.author_id:
            # We have updated unread counter through bulk update,
            # so increase counters in python
            interlocutor.unread += 1
            async_to_sync(channel_layer.group_send)(
                get_user_group_channel_name(interlocutor.user_id),
                format_websocket_message(
                    "interlocutor.unread",
                    InterlocutorUnreadSerializer(instance=interlocutor).data
                ),
            )

        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor.user_id),
            format_websocket_message("message.created", message_info),
        )


def receive_message(id: str, conversation_id: str, by: "UserModel"):
    """
    Process message receiving by one of the interlocutors.

    :param id: Message id.
    :param conversation_id: Conversation id.
    :param by: User instance.
    """

    with transaction.atomic():
        interlocutors = {
            _.user_id: _ for _ in Interlocutor.objects.filter(conversation=conversation_id)
        }
        interlocutor = interlocutors[by.id]

        message_status, status_was_changed = react_on_message(
            message_id=id,
            interlocutor_id=interlocutor.id,
            user_id=by.id,
            new_status=MESSAGE_STATUSES.received
        )

    if not status_was_changed:
        return

    channel_layer = get_channel_layer()

    serializer = MessageStatusUpdateSerializer(data={
        "message_id": str(id),
        "conversation_id": str(conversation_id),
        "user_id": by.id,
        "interlocutor_id": interlocutor.id
    })
    serializer.is_valid(raise_exception=True)
    event_data = serializer.data

    for interlocutor in interlocutors.values():
        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor.user_id),
            format_websocket_message("message.received", event_data),
        )


def read_message(id: UUID, conversation_id: UUID, by: int):
    """
    Process message reading by one of the interlocutors.

    :param id: Message id.
    :param conversation_id: Conversation id.
    :param by: User id.
    """

    with transaction.atomic():
        interlocutors = {
            _.user_id: _ for _ in Interlocutor.objects.filter(conversation=conversation_id)
        }

        interlocutor = interlocutors[by]

        message_status, status_was_changed = react_on_message(
            message_id=id,
            interlocutor_id=interlocutor.id,
            user_id=by,
            new_status=MESSAGE_STATUSES.read
        )

        if status_was_changed:
            interlocutor.unread -= 1
            interlocutor.save()

    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(
        get_user_group_channel_name(interlocutor.user_id),
        format_websocket_message(
            "interlocutor.unread",
            InterlocutorUnreadSerializer(instance=interlocutor).data
        ),
    )

    serializer = MessageStatusUpdateSerializer(data={
        "message_id": str(id),
        "conversation_id": str(conversation_id),
        "user_id": by,
        "interlocutor_id": interlocutor.id
    })
    serializer.is_valid(raise_exception=True)
    event_data = serializer.data

    for interlocutor in interlocutors.values():
        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor.user_id),
            format_websocket_message("message.read", event_data),
        )


def send_new_conversation(conversation: Conversation, interlocutors: List[int]):
    """
    Process new conversation.

    :param conversation: New conversation instance.
    :param interlocutors: List of message interlocutors as user id.
    """

    channel_layer = get_channel_layer()
    event_data = ConversationSerializer(instance=conversation).data

    for interlocutor in interlocutors:
        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor),
            format_websocket_message(
                "conversation.new",
                event_data,
            ),
        )


def delete_message(message_id: str, conversation_id: Union[str, UUID]):
    """
    Process message deleting by message author.

    :param message_id: Message id.
    :param conversation_id: Conversation id.
    """

    with transaction.atomic():
        # Delete message
        message: Message = Message.objects.filter(id=message_id).first()

        # Message was already deleted
        if message.deleted_at:
            return

        message.process_delete()
        message.save()

        # Update unread counter for all users who didn't read a message
        # except the author
        not_read_interlocutors = Interlocutor.objects.not_read_message(message.id, message.author_id)
        not_read_interlocutors.filter(unread__gt=0).update(unread=models.F("unread") - 1)
        not_read_interlocutors_ids = set(
            not_read_interlocutors.values_list("id", flat=True)
        )

        interlocutors = {
            _.user_id: _ for _ in
            Interlocutor.objects.filter(conversation=conversation_id)
        }
        interlocutor = interlocutors[message.author_id]
        # Set message status for author as deleted
        react_on_message(
            message_id,
            interlocutor_id=interlocutors[message.author_id].id,
            user_id=message.author_id,
            new_status=MESSAGE_STATUSES.deleted
        )

    channel_layer = get_channel_layer()

    serializer = MessageStatusUpdateSerializer(data={
        "message_id": str(message_id),
        "conversation_id": str(conversation_id),
        "user_id": message.author_id,
        "interlocutor_id": interlocutor.id
    })
    serializer.is_valid(raise_exception=True)
    event_data = serializer.data

    for interlocutor in interlocutors.values():
        if interlocutor.id in not_read_interlocutors_ids:
            async_to_sync(channel_layer.group_send)(
                get_user_group_channel_name(interlocutor.user_id),
                format_websocket_message(
                    "interlocutor.unread",
                    InterlocutorUnreadSerializer(instance=interlocutor).data
                ),
            )

        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor.user_id),
            format_websocket_message("message.deleted", event_data),
        )


def update_message(message: Message):
    """
    Process message update by message author.

    :param message: Message instance.
    """

    with transaction.atomic():
        message.edited_at = timezone.now()
        message.save()

        interlocutors = list(
            Interlocutor.objects.filter(conversation=message.conversation_id)
        )

    channel_layer = get_channel_layer()
    message_data = MessageSerializer(message).data

    for interlocutor in interlocutors:
        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(interlocutor.user_id),
            format_websocket_message(
                "message.updated", message_data
            ),
        )


def get_or_create_conversation_from_interlocutors(
    owner, *interlocutors: Tuple[id]
) -> Tuple[Conversation, bool]:
    """
    Returns:
        Tuple[Conversation, bool]: Conversation instance, new instance or not
    """

    # Owner is also an interlocutor in the chat
    if owner.id not in interlocutors:
        interlocutors += (owner.id,)

    # Check whether the conversation has no other interlocutors except given.
    # For example: *interlocutors = (1, 2), but conversation has (1, 2, 3, 4)
    conversation = Conversation.objects.with_only_interlocutors(
        interlocutors
    ).first()

    if conversation:
        return conversation, False

    with transaction.atomic():
        conversation = Conversation.objects.create(owner=owner)
        Interlocutor.objects.bulk_create(
            [
                Interlocutor(user_id=interlocutor, conversation=conversation)
                for interlocutor in interlocutors
            ]
        )

    return conversation, True


def react_on_message(
    message_id: Union[str, UUID],
    interlocutor_id: Union[str, UUID],
    user_id: int,
    new_status: int
) -> Tuple[MessageStatus, bool]:
    """
    Updates interlocutor's status for a given message.

    :param message_id:
    :param interlocutor_id:
    :param user_id:
    :param new_status:
    """

    message_status: MessageStatus = MessageStatus.objects.filter(
        message_id=message_id,
        interlocutor_id=interlocutor_id,
    ).first()

    if not message_status:
        message_status = MessageStatus.objects.create(
            message_id=message_id,
            interlocutor_id=interlocutor_id,
            user_id=user_id,
            status=new_status
        )

        # There was no user status for a message, so no change
        return message_status, False

    if message_status.status > new_status:
        return message_status, False

    message_status.status = new_status
    message_status.save()

    return message_status, True


def notify_user_status_changed(user_id, new_status: str):
    """
    Send notification to users about user new status.

    :param user_id: User's id whose status was changes.
    :param new_status: New user's status
    """

    if not NOTIFY_ALL_USER_STATUS_CHANGE:
        user_conversations = Interlocutor.objects.filter(user_id=user_id).values_list('conversation_id', flat=True)
        to_notify = Interlocutor.objects.filter(conversation_id__in=user_conversations).exclude(user_id=user_id).values_list('user_id', flat=True).distinct()
    else:
        to_notify = UserModel.objects.exclude(id=user_id).values_list('id', flat=True)

    channel_layer = get_channel_layer()

    serializer = UserStatusChangedSerializer(data={
        'user_id': user_id,
        'new_status': new_status,
        'changed_at': timezone.now()
    })
    serializer.is_valid(raise_exception=True)

    for user in to_notify:
        async_to_sync(channel_layer.group_send)(
            get_user_group_channel_name(user),
            format_websocket_message("user.changed", serializer.data),
        )

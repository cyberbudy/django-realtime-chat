from typing import Dict, List

from asgiref.sync import sync_to_async
from channels.layers import get_channel_layer

from .const import MESSAGE_STATUSES
from .utils import get_user_group_channel_name, format_websocket_message
from .models import Interlocutor, Message
from .services import (
    read_message,
    receive_message,
    send_new_message,
    send_new_conversation,
    get_or_create_conversation_from_interlocutors
)
from .serializers import (
    NewTextMessageSerializer,
    MessageStatusChangeSerializer,
    UserEventMessageSerializer
)


class EventHandler:
    """
    Base class for websocket event processing.

    Attributes:
        serializer_class: Serializer class to validate websocket data.
    """

    serializer_class = None

    async def get_serializer_class(self, message: Dict, scope: Dict, extra_context):
        """
        Returns initialized serializer class with proper context.

        :param message: Message data passed to serializer.
        :param scope: Consumer's scope.
        :param extra_context: Extra context passed to serializer.

        :return: Serializer object.
        """

        assert self.serializer_class, f"{self.__class__.__name__} has no `serializer_class`"

        context = await self.get_serializer_context(
            scope=scope, user=scope['user'], **extra_context
        )

        return self.serializer_class(data=message, context=context)

    async def get_serializer_context(self, **kwargs) -> Dict:
        """
        Returns serializer context.

        :param kwargs:
        :return: Context dict.
        """

        return kwargs

    async def __call__(self, scope: Dict, type: str, body: Dict, **kwargs):
        serializer = await self.get_serializer_class(body, scope, kwargs)

        # User may call database or other only sync operations during validation
        await sync_to_async(serializer.is_valid)()

        await self.perform_event(scope, type, serializer.data, **kwargs)

    async def perform_event(self, scope: Dict, type: str, body: Dict, **kwargs):
        """
        Put your login here. Will be called after validating message with serializer,

        :param scope: Consumer's scope.
        :param type: Message event type.
        :param body: Validated message data from serializer.
        :param kwargs: Extra data from consumer.
        """

        raise NotImplementedError()


class SimpleEventPropagator(EventHandler):
    """
    Propagates event from interlocutor to interlocutor, for example for
    typing events.
    """

    serializer_class = UserEventMessageSerializer

    async def perform_event(self, scope: Dict, type: str, body: Dict, **kwargs):
        interlocutors = await sync_to_async(get_conversation_interlocutors)(
            body['conversation_id'], scope['user'].id
        )

        channel_layer = get_channel_layer()

        for interlocutor in interlocutors:
            await channel_layer.group_send(
                get_user_group_channel_name(interlocutor.user_id),
                format_websocket_message(type, body)
            )


def get_conversation_interlocutors(conversation_id, user_id) -> List[Interlocutor]:
    """
    Returns conversation interlocutors.

    :param conversation_id: Conversation id.
    :param user_id: User id.
    :return: List of conversation interlocutor.
    """
    return list(
        Interlocutor
            .objects
            .filter(conversation=conversation_id)
            .exclude(user_id=user_id)
    )


class NewMessageHandler(EventHandler):
    """
    Handler for a new message.
    """

    serializer_class = NewTextMessageSerializer

    async def perform_event(self, scope: Dict, type: str, body: Dict, **kwargs):
        conversation = body.get('conversation_id')

        if not conversation:
            conversation, was_created = await sync_to_async(get_or_create_conversation_from_interlocutors)(
                scope["user"], *body['interlocutors']
            )
            if was_created:
                await sync_to_async(send_new_conversation)(conversation, [*body['interlocutors'], scope['user'].id])

            conversation = conversation.id

        message = Message(
            conversation_id=conversation,
            author=scope['user'],
            text=body.get("content"),
        )

        await sync_to_async(send_new_message)(message)


class ReadMessageHandler(EventHandler):
    """
    Handler for a read message status change.
    """

    serializer_class = MessageStatusChangeSerializer

    async def get_serializer_context(self, **kwargs):
        context = await super().get_serializer_context(**kwargs)
        context["new_status"] = MESSAGE_STATUSES.read

        return context

    async def perform_event(self, scope: Dict, type: str, body: Dict, **kwargs):
        await sync_to_async(read_message)(
            body.get("message_id"),
            body.get("conversation_id"),
            scope["user"].id,
        )


class ReceiveMessageHandler(EventHandler):
    """
    Handler for a reaceive message status change.
    """
    serializer_class = MessageStatusChangeSerializer

    async def get_serializer_context(self, **kwargs):
        context = await super().get_serializer_context(**kwargs)
        context["new_status"] = MESSAGE_STATUSES.read

        return context

    async def perform_event(self, scope: Dict, type: str, body: Dict, **kwargs):
        await sync_to_async(receive_message)(
            body.get("message_id"),
            body.get("conversation_id"),
            scope["user"],
        )

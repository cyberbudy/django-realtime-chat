from datetime import datetime
from typing import TYPE_CHECKING, Dict, Union
from uuid import UUID
import json

try:
    import orjson
except ImportError:  # pragma: nocover
    orjson = None  # type: ignore

import pytz
import humps

from .conf import MESSAGE_PREFIX

if TYPE_CHECKING:
    from django.contrib.auth import get_user_model

    UserModel = get_user_model()


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)


def get_user_group_channel_name(user: Union["UserModel", int]):
    if isinstance(user, int):
        return f"channel__{user}"
    else:
        return f"channel__{user.id}"


def format_websocket_message(type: str, body: Dict) -> Dict:
    return {
        "type": f"{MESSAGE_PREFIX}.{type}",
        "created_at": (
            datetime.utcnow()
            .replace(tzinfo=pytz.utc)
            .isoformat()
            .replace("+00:00", "Z")
        ),
        "body": body,
    }


class JSONParser:

    def parse(self, text_data: str) -> Dict:
        return humps.decamelize(json.loads(text_data))


class ORJSONParser():

    def parse(self, text_data: str) -> Dict:
        assert orjson is not None, "orjson must be installed to use ORJSONParser"
        return humps.decamelize(orjson.loads(text_data))


class JSONRenderer:

    def render(self, message: Dict) -> str:
        # Default json library cannot serialize uuid type
        return json.dumps(humps.camelize(message), cls=UUIDEncoder)


class ORJSONRenderer:

    def render(self, message: Dict) -> str:
        assert orjson is not None, "orjson must be installed to use ORJSONRenderer"
        return orjson.dumps(humps.camelize(message)).decode()


def get_event_name_without_prefix(event: str) -> str:
    to_strip = len(MESSAGE_PREFIX) + 1

    if not event or len(event) < to_strip:
        return ""

    return event[to_strip:]

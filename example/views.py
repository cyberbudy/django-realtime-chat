from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.contrib.auth import get_user_model
from standards.drf.views import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from . import settings
from .serializers import UserSerializer


class IndexView(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(f"{settings.LOGIN_URL}?next=/")

        return super().get(request, *args, **kwargs)


class UserInfoView(RetrieveAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user

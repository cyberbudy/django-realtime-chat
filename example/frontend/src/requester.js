import axios from "axios";

const USER_SEARCH_PATH = "chat/search/user",
  CONVERSATION_LIST = "chat/conversations/list",
  USER_INFO = "user/info",
  CONVERSATION_MESSAGES = "chat/messages/list";

export default class Requester {
  constructor() {
    this.baseUrl = `/api/v1/`;
  }
  constructUrl(path) {
    return `${this.baseUrl}${path}`;
  }
  searchUser(name) {
    return axios.get(this.constructUrl(USER_SEARCH_PATH), {
      params: {
        search: name
      }
    });
  }
  conversationList(searchValue, pageSize, cursor) {
    let params = {
      page_size: pageSize,
      search: searchValue,
      cursor: cursor
    };

    return axios.get(this.constructUrl(CONVERSATION_LIST), {
      params: params
    });
  }
  conversationMessages(conversationId, cursor, pageSize = 10) {
    let params = {
      page_size: pageSize,
      conversation_id: conversationId,
      cursor: cursor
    };
    return axios.get(this.constructUrl(CONVERSATION_MESSAGES), {
      params: params
    });
  }
  userInfo() {
    return axios.get(this.constructUrl(USER_INFO));
  }
}

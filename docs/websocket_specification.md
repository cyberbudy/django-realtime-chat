```yaml
definitions:
  event:
    name: Event
    description: Schema of the event object to work with.

    type: object
    properties:
      type:
        $ref: '#/components/schemas/eventTypes'
      createdAt:
        type: string
        description: UTC ISO time string.
      body:
        type: object
        description: Body scheme depends on event type.
        oneOf:
          - InterlocutorUnreadCountChanged
          - MessageStatusChanged
          - NewMessage
          - UserStatusChange
          - NewConversation

components:
  schemas:
    InterlocutorUnreadCountChanged:
      type: object
      properties:
        id:
          type: uuid
          description: Interlocutor id
        userId:
          type: int
          description: Interlocutor's user id
        conversationId:
          type: uuid
          description: Interlocutor's conversation
        unread:
          type: int
          description: New unread messages number of interlocutor in convresation
    MessageStatusChanged:
      type: object
      properties:
        messageId:
          type: uuid
          description: Message id
        userId:
          type: int
          description: Interlocutor's user id
        conversationId:
          type: uuid
          description: Interlocutor's conversation whose message status was changed
        interlocutorId:
          type: uuid
          description: Interlocutor's id
    NewMessage:
      type: object
      description: Same as REST API message list instance
    UserStatusChange:
      userId:
        type: int
        description: Interlocutor's user id
      newStatus:
        type: string
        description: User new status - online/offline
      changedAt:
        type: datetime
        description: When the user status was changed
    NewConversation:
      type: object
      description: Same as REST API conversation instance

    eventTypes:
      type: string
      enum:
        - chat.typing.start
        - chat.typing.stop
        - chat.message.created
        - chat.message.read
        - chat.message.received
        - chat.user.changed
        - chat.conversation.new
        - chat.message.deleted

```

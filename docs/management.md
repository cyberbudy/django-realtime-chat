#### Management commands

You have one command `init_chat` - creates extra objects required to work with users inside chat.

This may be usefull if you integrate chat in existing application with a large users database to reduce the amount of extra queries.

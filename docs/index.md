## Introduction

This package aims to provide a simple and customizable chat powered by django and django-channels.

It gives you ability to create one-to-one chat, group chat, sending text and file messages.

Messaging is supported only between authenticated users .

#### Requirements

- Python 3.6+
- Django 2.1+
- Django-channels 2.0+

#### Instalation

``$ pip install django-realtime-chat``

Add package to the settings:

````python
INSTALLED_APPS += (
    "realtime_chat",
    "channels"  # channels is required, obviously
)
````

Set up your ``ASGI_APPLICATION``

```python
ASGI_APPLICATION = "myproject.routing.application"
```

Include chat consumer in your ``project/routing.py``

```python
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack

from realtime_chat import routing

application = ProtocolTypeRouter(
    {
        "websocket": AuthMiddlewareStack(
            URLRouter(routing.websocket_urlpatterns)
        ),
    }
)
```

or if your want more control

```python
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from django.urls import path

from realtime_chat import consumers

application = ProtocolTypeRouter(
    {
        "websocket": AuthMiddlewareStack(
            URLRouter([path("your-path-to-ws/", consumers.ChatConsumer),])
        ),
    }
)
```

If you want to increase json processing performance install [orjson](https://pypi.org/project/orjson/) and set in your settings

```python
CHAT_MESSAGE_RENDERER = "realtime_chat.utils.ORJSONRenderer"
```

!!GET /chat/conversations/list!!

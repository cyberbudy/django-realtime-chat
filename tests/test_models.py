from io import BytesIO

import pytest
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile

from realtime_chat import models, const


@pytest.mark.django_db
def test_model_methods():
    UserModel = get_user_model()
    user = UserModel.objects.create(username="ad", email="ad@ad.ad")

    conversation = models.Conversation.objects.create(title="title",)
    assert isinstance(conversation.__str__(), str)

    interlocutor = models.Interlocutor.objects.create(
        user=user, conversation=conversation
    )
    assert isinstance(interlocutor.__str__(), str)

    user_status = models.UserStatus.objects.get(user=user)
    assert user_status.status == "offline"
    assert isinstance(user_status.__str__(), str)

    text_message = models.Message.objects.create(
        conversation=conversation, author=user, text="text",
    )

    assert isinstance(text_message.__str__(), str)
    assert text_message.is_image is False

    img = SimpleUploadedFile("myimage.jpg", b"mybinarydata")
    file_message = models.Message.objects.create(
        conversation=conversation, author=user, file=img,
    )
    # assert file_message.file != None
    assert isinstance(text_message.__str__(), str)
    assert file_message.is_image is True

    message_status = models.MessageStatus.objects.create(
        message=text_message,
        user=user,
        status=const.MESSAGE_STATUSES.created,
        interlocutor=interlocutor,
    )
    assert isinstance(message_status.__str__(), str)

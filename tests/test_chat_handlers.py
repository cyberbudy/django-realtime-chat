from datetime import datetime

import pytest
from asgiref.sync import sync_to_async
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator

import realtime_chat.consumers
import realtime_chat.conf
from realtime_chat.const import MESSAGE_STATUSES
from realtime_chat.consumers import ChatConsumer
from realtime_chat.models import Interlocutor, MessageStatus


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_new_message_event(test_users):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_users[0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_users[1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()
    assert connected
    await communicator.send_json_to({
        "type": "chat.message.created",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "content": "message",
            "interlocutors": [x.id for x in test_users]
        }
    })
    response = await communicator.receive_json_from()

    assert response.get('type') == "chat.conversation.new"

    # Each user must receive a new message
    # for i in test_users:
    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.conversation.new"

    response = await communicator.receive_json_from()
    assert response.get('type') == "chat.conversation.new"

    response = await communicator.receive_json_from()
    assert response.get('type') == "chat.message.created"

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.interlocutor.unread"

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.message.created"

    interlocutor1 = await sync_to_async(Interlocutor.objects.get)(user=test_users[1])
    interlocutor = await sync_to_async(Interlocutor.objects.get)(user=test_users[0])

    assert 1 == interlocutor1.unread
    assert 0 == interlocutor.unread



@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_typing_events(test_conversation):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()
    assert connected
    await communicator.send_json_to({
        "type": "chat.typing.start",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id)
        }
    })

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.typing.start"

    await communicator.send_json_to({
        "type": "chat.typing.stop",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id)
        }
    })

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.typing.stop"


@pytest.mark.asyncio
@pytest.mark.django_db(transaction=True)
async def test_read_message_event(test_conversation):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()
    assert connected
    await communicator1.send_json_to({
        "type": "chat.message.read",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id),
            "messageId": str(test_conversation['message'].id)
        }
    })

    response = await communicator1.receive_json_from()

    assert response.get('type') == "chat.interlocutor.unread"

    response = await communicator.receive_json_from(5)
    assert response.get('type') == "chat.message.read"

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.message.read"
    message = await database_sync_to_async(MessageStatus.objects.get)(
        message_id=test_conversation['message'].id,
        user_id=test_conversation['users'][1].id
    )

    assert message.status == MESSAGE_STATUSES.read

    interlocutor = await database_sync_to_async(Interlocutor.objects.get)(
        user=test_conversation["users"][1], conversation=test_conversation["conversation"]
    )
    assert 0 == interlocutor.unread

    await communicator1.send_json_to({
        "type": "chat.message.read",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id),
            "messageId": str(test_conversation['message'].id)
        }
    })


    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.error"

    interlocutor = await database_sync_to_async(Interlocutor.objects.get)(
        user=test_conversation["users"][1], conversation=test_conversation["conversation"]
    )
    assert 0 == interlocutor.unread

    await communicator.send_json_to({
        "type": "chat.message.read",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id),
            "messageId": str(test_conversation['message'].id)
        }
    })

    response = await communicator.receive_json_from()
    interlocutor = await database_sync_to_async(Interlocutor.objects.get)(
        user=test_conversation["users"][0], conversation=test_conversation["conversation"]
    )
    assert 0 == interlocutor.unread



@pytest.mark.asyncio
@pytest.mark.django_db(transaction=True)
async def test_receive_message_event(test_conversation):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()
    assert connected
    await communicator1.send_json_to({
        "type": "chat.message.received",
        "createdAt": datetime.utcnow().isoformat(),
        "body": {
            "conversationId": str(test_conversation['conversation'].id),
            "messageId": str(test_conversation['message'].id)
        }
    })

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.message.received"

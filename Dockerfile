# Python version
FROM python:3.7
ENV PYTHONUNBUFFERED=1

WORKDIR /code

# Install gettext to compile tranlations
RUN apt-get update && apt-get install gettext -y

# Install pipenv
RUN pip install --upgrade pip && pip install poetry

# Copy pipfile and install it as a system packages
COPY ./poetry.lock ./pyproject.toml  /code/

# No need in virtualenv as we are already inside container
RUN poetry config virtualenvs.create false && poetry install

# Copy project files only here, because we don't want to reinstall
# requirements each time file is changed.
COPY ./ /code
